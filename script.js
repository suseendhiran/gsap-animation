// gsap.from(".header", { duration: 1, y: "-100%", ease: "expo" });
// gsap.from(".link", { duration: 1, opacity: 0, delay: 1, stagger: 0.3 });
// gsap.from(".right", { duration: 1, x: "-100vw", ease: "expo", delay: 2 });
// gsap.from(".left", { duration: 1, x: "100vw", ease: "expo", delay: 2 });
// gsap.to(".footer", { duration: 1, ease: "expo", y: 0, delay: 3 });
// gsap.fromTo(
//   ".button",
//   { opacity: 0, scale: 0, rotation: 1440 },
//   { duration: 1, delay: 0, opacity: 1, scale: 1, rotation: 0, delay: 3 }
// );

const timeline = gsap.timeline({ defaults: { duration: 1 } });

timeline
  .from(".header", { y: "-100%", ease: "power2.in" })
  .from(".link", { opacity: 0, stagger: 0.3 })
  .from(".right", { duration: 1.4, x: "-100vw", ease: "power2.in" }, 1)
  .from(".left", { duration: 1.4, x: "100vw", ease: "power2.in" }, 1)
  .to(".footer", { ease: "expo", y: 0 }, 1.2)
  .fromTo(
    ".button",
    { opacity: 0, scale: 0, rotation: 720 },
    { opacity: 1, scale: 1, rotation: 0 }
  );

const button = document.querySelector(".button");
button.addEventListener("click", () => {
  timeline.reverse();
});
